<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Autre moyen d'appeler une page, fonctionne en GET
Route::view('/accueil', 'accueil');

// Dans produit.frais, le "." est égale à "/", c'est une convention de laravel
Route::get('/produits', function () {
    return view('produits.index',[
        "produit" => "Lait",
        "date" => "date('d/m/y')",
        "alphabet" => range('a','z')
    ]);
});

Route::get('/produits/frais', function () {
    return view('produits.frais');
});

Route::post('/produits/frais', function () {
    return view('produits.post');
});
