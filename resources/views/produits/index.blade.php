@extends('layout')

@section('title', 'Page des produits')
{{-- Debut du contenu --}}
@section('content')
     <h2>Vous êtes sur la page produit</h2>
     {{-- La double { execute un htmlspecialchars --}}
     <p>Voici le {{ $produit }}</p>
     <p>Nous sommes le {{ date('d/m/y') }}</p>

     <ul>
        @foreach ($alphabet as $lettre)
            <li>Lettre : {{ $lettre }}</li>
        @endforeach
        </ul>

@endsection
{{-- Fin du contenu --}}

